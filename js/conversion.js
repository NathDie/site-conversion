function Fonc_decimal() {
    decimal = document.getElementById("txt_decimal");
    hexadecimal = document.getElementById("txt_hexadecimal");
    binaire = document.getElementById("txt_binaire");

    hexadecimal.value = (decimal.value - 0).toString(16);
    binaire.value = (decimal.value - 0).toString(2);
}

function Fonc_hexadecimal() {
    decimal = document.getElementById("txt_decimal");
    hexadecimal = document.getElementById("txt_hexadecimal");
    binaire = document.getElementById("txt_binaire");

    decimal.value = parseInt(hexadecimal.value, 16);
    binaire.value = (parseInt(hexadecimal.value, 16)).toString(2);
}

function Fonc_binaire() {
    decimal = document.getElementById("txt_decimal");
    hexadecimal = document.getElementById("txt_hexadecimal");
    binaire = document.getElementById("txt_binaire");

    decimal.value = parseInt(binaire.value, 2);
    hexadecimal.value = (parseInt(binaire.value, 2)).toString(16);
}